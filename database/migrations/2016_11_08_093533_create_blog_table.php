<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('blogs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->decimal('decimal', 5, 2)->nullable();
            $table->double('double', 15, 8)->nullable();
            $table->enum('enum', ['foo', 'bar'])->nullable();
            $table->float('float', 8, 2)->nullable();
            $table->bigInteger('bigInteger')->nullable();
            $table->binary('binary')->nullable();
            $table->ipAddress('ipAddress')->nullable();
            $table->unique('email')->nullable();
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->nullableTimestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('blogs');
    }
}
