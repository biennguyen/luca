<?php

namespace Antking\Generator\Console\Generators;

use Caffeinated\Modules\Console\GeneratorCommand;
use Antking\Generator\Libraries\SchemaGenerator;
use Illuminate\Support\Str;
use Illuminate\Filesystem\Filesystem;

class MakeModelCommand extends GeneratorCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ant:gen:model
    	{slug : The slug of the module.}
    	{name : The name of the model class.}
        {--migration : Create a new migration file for the model.}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new module model class';

    /**
     * String to store the command type.
     *
     * @var string
     */
    protected $type = 'Module model';

    /**
     * The filesystem instance.
     *
     * @var Filesystem
     */
    protected $files;

    /**
     * @var \Antking\Generator\Libraries\SchemaGenerator;
     */
    protected $schemaGenerator;

    protected $params= [
        'class' => '' ,
        'table_name' => '' ,
        'properties' => '',
        'property_read' => '',
        'methods' => '',
        'time_stamps' => '',
        'soft_delete' => '',
        'dates' => '',
        'fillable' => '',
        'guarded' => '',
        'hidden' => '',
        'rules' => '',
        'relations' => ''
    ];

    /**
     * Create a new command instance.
     *
     * @param Filesystem $files
     * @param Modules    $module
     */
    public function __construct(Filesystem $files)
    {
        parent::__construct($files);

        $this->files  = $files;
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function fire()
    {
        if (parent::fire() !== false) {
            $name = Str::snake(class_basename($this->argument('name')));
            $table = Str::plural($name);
            $this->params['class'] = ucfirst(Str::camel($name));
            if ($this->option('migration')) {
                $this->call('make:module:migration', [
                    'slug'     => $this->argument('slug'),
                    'name'     => "create_{$table}_table",
                    '--create' => $table
                ]);
            }
            $this->getTableStructure($table);
            if ($this->alreadyExists($this->getNameInput())) {

                $name = $this->parseName(ucfirst(trim($this->argument('name'))));
                logger('params',$this->params);
                $path = $this->getPath($name);
                $content = $this->files->get($path);
                $content = $this->replacePlaceholders($content);
                $this->files->put($path, $content);
            }
        }
    }

    protected function getTableStructure($table_name){

        $this->schemaGenerator = new SchemaGenerator(
            config('database.default'),
            true,
            true
        );

        $this->params['table_name']= $table_name;
        $fields = $this->schemaGenerator->getFields($table_name);

        if($fields){
            logger('fields',$fields);
            $this->makeTimeStamps($fields);
            $this->makeSoftDelete($fields);
            $this->makeOthers($fields);
        }
        // relation n-1
        $keys = $this->schemaGenerator->getForeignKeyConstraints($table_name);
        $this->makeRelations($table_name);
    }

    protected function makeRelations($table_name){
        // relation 1-n
        $source    =  __DIR__.'/stubs/relations.stub';
        $content = $this->files->get($source);
        $tables  =  $this->schemaGenerator->getTables();
        $foreign_key = [];
        $property_read = [];
        foreach ($tables as $table){
            $keys = $this->schemaGenerator->getForeignKeyConstraints($table);
            foreach($keys as $key => $fk){
                if($fk['on'] === $table_name){
                    $class = str_replace(' ', '', ucwords(str_replace('_', ' ', Str::singular($table))));
                    $foreign_key [] = str_replace('DummyRelate', $class, str_replace('DummyRelateFunction',lcfirst($class) ,str_replace('DummyForeignKey', $fk['field'] ,str_replace('DummyRelateNameSpace', $this->getCurrentNameSpace() , $content))));
                    $property_read [] = ' * @property-read \Illuminate\Database\Eloquent\Collection|' . $this->getCurrentNameSpace().'\\'.$class . '[] $' . lcfirst(Str::plural($class));
                }
            }
        }
        logger('foreign Keys:', $foreign_key);
        $this->params['relations'] = implode(PHP_EOL, $foreign_key);
        $this->params['property_read'] = implode(PHP_EOL, $property_read);
    }
    /**
     * @param $fields
     *
     */
    protected function makeSoftDelete($fields){
        if(array_key_exists('deleted_at',$fields)){
            $source    =  __DIR__.'/stubs/softdelete.stub';
            $this->params['soft_delete'] = $this->files->get($source);
        }
    }
    /**
     * @param $fields
     *
     */
    protected function makeOthers($fields){
        $dates = [];
        $this->params['rules'] = 'public static $rules = [';
        $rules = [];
        $properties = [];
        $methods = [];
        foreach ($fields as $key =>$field){
            $camel_key = str_replace(' ', '', ucwords(str_replace('_', ' ', $key)));
            $methods []=' * @method static \Illuminate\Database\Query\Builder|'.$this->getCurrentNameSpace().'\\'.$this->params['class'].' where'.$camel_key.'($value)';
            if($field['type'] === 'dateTime'){
                $dates []= '"'. $key . '"';
                $properties[] = ' * @property \Carbon\Carbon $'.$key;
            }
            else if($field['type'] !== 'increments'){
                $rule = [];
                if($field['type']=='string'){
                    $properties[] = ' * @property string $'.$key;
                    $rule[]= 'string';
                    $rule[]= 'max:'.$field['length'];
                }
                if(!array_key_exists('decorators',$field)){
                    $rule[]= 'require';
                }
                elseif($field['decorators'] && is_array($field['decorators']) && in_array('unique',$field['decorators'])){
                    $rule[]= 'unique:'.$this->params['table_name'].','.$key;
                }
                $rules[] = "'" . $key . "' =>'".implode('|',$rule)."'";
            }
            else{
                $properties[] = ' * @property integer $'.$key;
            }
        }
        $this->makeMethods($methods);
        $this->makeProperties($properties);
        $this->makeRules($rules);
        $this->makeDates($dates);
    }

    /**
     * @param $methods
     */
    function makeMethods($methods ){
        logger('$methods:', $methods );
        $methods_string = '';
        if(count($methods) > 0){
            $methods_string = implode(PHP_EOL, $methods);
        }
        $this->params['methods'] = $methods_string;
    }
    /**
     * @param $properties
     */
    function makeProperties($properties ){
        logger('$properties:', $properties );
        $properties_string = '';
        if(count($properties) > 0){
            $properties_string = implode(PHP_EOL , $properties);
        }
        $this->params['properties'] = $properties_string;
    }
    /**
     * @param $rules
     */
    function makeRules($rules){
        logger('rules:', $rules );
        $rules_string='';
        if(count($rules) > 0){
            $rules_string = '[' . PHP_EOL . "\t\t\t\t\t" . implode(',' . PHP_EOL . "\t\t\t\t\t", $rules) . PHP_EOL . "\t\t\t\t" . ']';
        }
        else{
            $rules_string = '[]';
        }
        $source    =  __DIR__.'/stubs/rules.stub';
        $content = $this->files->get($source);
        $content = str_replace('DummyRules', $rules_string, $content);
        $this->params['rules'] = $content;
    }

    /**
     * @param $dates
     */
    function makeDates($dates ){
        logger('dates:', $dates );
        $dates_string='';
        if(count($dates) > 0){
            $dates_string = '[' . implode(',',$dates) . ']';
        }
        else{
            $dates_string = '[]';
        }
        $source    =  __DIR__.'/stubs/dates.stub';
        $content = $this->files->get($source);
        $content = str_replace('DummyDates', $dates_string, $content);
        $this->params['dates'] = $content;
    }

    /**
     * @param $fields
     *
     */
    protected function makeTimeStamps($fields){
        if(array_key_exists('created_at',$fields) && array_key_exists('updated_at',$fields)){
            $source    = __DIR__.'/stubs/timestamps.stub';
            $this->params['time_stamps'] = $this->files->get($source);
        }
    }

    /**
     * Get the stub file for the generator.
     *
     * @return string
     */
    protected function getStub()
    {
        return __DIR__.'/stubs/model.stub';
    }

    /**
     * Get the default namespace for the class.
     *
     * @param  string  $rootNamespace
     * @return string
     */
    protected function getDefaultNamespace($rootNamespace)
    {
        return module_class(lcfirst($this->argument('slug')), 'Models');
    }

    protected function replacePlaceholders($contents)
    {
        $find = [
            'DummyClass',
            'DummyTable',
            'DummyProperties',
            'DummyPropertyRead',
            'DummyMethods',
            'DummyTimeStamps',
            'DummySoftDelete',
            'DummyDates',
            'DummyFillable',
            'DummyGuarded',
            'DummyHidden',
            'DummyRules',
            'DummyRelations'
        ];

        $replace = [
            $this->params['class'],
            $this->params['table_name'],
            $this->params['properties'],
            $this->params['property_read'],
            $this->params['methods'],
            $this->params['time_stamps'],
            $this->params['soft_delete'],
            $this->params['dates'],
            $this->params['fillable'],
            $this->params['guarded'],
            $this->params['hidden'],
            $this->params['rules'],
            $this->params['relations']
        ];

        return str_replace($find, $replace, $contents);
    }

    protected function getCurrentNameSpace()
    {
        return parent::getNamespace($this->parseName(trim($this->argument('name'))));
    }

    /**
     * Get the destination class path.
     *
     * @param  string  $name
     * @return string
     */
    protected function getPath($name)
    {
        $rootNamespace = config('modules.namespace');
        $name = str_replace($rootNamespace, '', $name);
        return module_path().'/'.str_replace('\\', '/', Str::ucfirst(Str::camel($name))).'.php';
    }
    /**
     * Replace the class name for the given stub.
     *
     * @param  string  $stub
     * @param  string  $name
     * @return string
     */
    protected function replaceClass($stub, $name)
    {
        $class = str_replace($this->getNamespace($name).'\\', '', $name);

        return str_replace('DummyClass', Str::ucfirst(Str::camel($class)), $stub);
    }

    /**
     * Get the desired class name from the input.
     *
     * @return string
     */
    protected function getNameInput()
    {
        return ucfirst(trim($this->argument('name')));
    }
}
