<?php

namespace Antking\Generator\Console\Generators;

use Caffeinated\Modules\Console\GeneratorCommand;
use Illuminate\Support\Str;

class MakeControllerCommand extends GeneratorCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ant:gen:controller
    	{slug : The slug of the module}
    	{name : The name of the controller class}
    	{--not_resource : Generate a module resource controller class}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new module controller class';

    /**
     * String to store the command type.
     *
     * @var string
     */
    protected $type = 'Module controller';

    /**
     * Execute the console command.
     *
     * @return bool|null
     */
    public function fire()
    {
        $this->addRoute($this->parseName($this->getNameInput()));
        parent::fire();
    }

    /**
     * @param $name
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    protected function addRoute($name){
        $source    =  __DIR__.'/stubs/routes/resource.stub';
        $replace_content = $this->files->get($source);
        $replace_content = str_replace('DummyNameSlug', Str::slug($this->argument('name')), $this->replaceClass($replace_content, $name));
        $module_api_route_path = $this->getModuleApiRoutePath();
        $provider_content = $this->files->get($module_api_route_path);
        logger(strpos($provider_content, $replace_content));
        logger($module_api_route_path);
        if(strpos($provider_content, $replace_content) === FALSE){
            $content = str_replace('//DummyMoreResources', $replace_content, $provider_content);
            $this->files->put($module_api_route_path, $content);
        }
    }


    /**
     * Get the destination Module Provider path.
     *
     * @return string
     */
    protected function getModuleApiRoutePath()
    {
        return module_path().'/'. Str::ucfirst($this->argument('slug')) . '/Routes/api.php';
    }
    /**
     * Get the stub file for the generator.
     *
     * @return string
     */
    protected function getStub()
    {
        if ($this->option('not_resource')) {
            return __DIR__.'/stubs/controller/controller.stub';
        }
        return __DIR__.'/stubs/controller/controller.resource.stub';
    }

    /**
     * Get the default namespace for the class.
     *
     * @param  string  $rootNamespace
     * @return string
     */
    protected function getDefaultNamespace($rootNamespace)
    {
        return module_class(Str::slug($this->argument('slug')), 'Http\\Controllers');
    }

    /**
     * Get the destination class path.
     *
     * @param  string  $name
     * @return string
     */
    protected function getPath($name)
    {
        $name = str_replace_first($this->laravel->getNamespace(), '', $name);

        return $this->laravel['path'].'/'.str_replace('\\', '/', Str::ucfirst(Str::camel($name))).'Controller.php';
    }
    /**
     * Replace the namespace for the given stub.
     *
     * @param  string  $stub
     * @param  string  $name
     * @return $this
     */
    protected function replaceNamespace(&$stub, $name)
    {
        $stub = str_replace(
            'DummyNamespace', $this->getNamespace($name), $stub
        );

        $stub = str_replace(
            'DummyRootNamespace', $this->laravel->getNamespace(), $stub
        );
        $stub = str_replace(
            'DummyModuleNamespace', module_class(Str::slug($this->argument('slug')),''), $stub
        );

        return $this;
    }

    /**
     * Replace the class name for the given stub.
     *
     * @param  string  $stub
     * @param  string  $name
     * @return string
     */
    protected function replaceClass($stub, $name)
    {
        $class = str_replace($this->getNamespace($name).'\\', '', $name);

        return str_replace('DummyClass',ucfirst(Str::camel($class)), $stub);
    }

    /**
     * Replace the class name for the given stub.
     *
     * @param  string  $stub
     * @param  string  $name
     * @return string
     */
    protected function replaceSlug($stub, $name)
    {
        $class = str_replace($this->getNamespace($name).'\\', '', $name);

        return str_replace('DummySlug',lcfirst(Str::slug($class,'_')), $stub);
    }
    /**
     * Get the desired class name from the input.
     *
     * @return string
     */
    protected function getNameInput()
    {
        return ucfirst(trim($this->argument('name')));
    }

    /**
     * Build the class with the given name.
     *
     * @param  string  $name
     * @return string
     */
    protected function buildClass($name)
    {
        $stub = $this->files->get($this->getStub());
        $stub = $this->replaceNamespace($stub, $name)->replaceClass($stub, $name);
        return $this->replaceSlug($stub, $name);
    }
}
