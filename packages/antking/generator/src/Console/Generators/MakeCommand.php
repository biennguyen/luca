<?php

namespace Antking\Generator\Console\Generators;

use Illuminate\Console\Command;

class MakeCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ant:gen
    	{slug : The slug of the module.}
    	{name : The name of the model class.}
        {--not_resource : Create a new migration file for the model.}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new module with all controller, model, repository, route';

    /**
     * String to store the command type.
     *
     * @var string
     */
    protected $type = 'Module and Controller';


    /**
     * Execute the console command.
     *
     * @return bool|null
     */
    public function fire()
    {
        $this->call('ant:gen:module', [
            'slug' => $this->argument('slug'), '--quick' => true
        ]);
        $this->call('ant:gen:controller', [
            'slug' => $this->argument('slug'), 'name' => $this->argument('name'), '--not_resource'=>$this->option('not_resource')
        ]);
        $this->call('ant:gen:repository', [
            'slug' => $this->argument('slug'), 'name' => $this->argument('name'), '--not_resource'=>$this->option('not_resource')
        ]);
        $this->call('ant:gen:model', [
            'slug' => $this->argument('slug'), 'name' => $this->argument('name')
        ]);
    }
}
