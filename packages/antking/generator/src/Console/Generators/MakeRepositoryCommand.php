<?php

namespace Antking\Generator\Console\Generators;

use Caffeinated\Modules\Console\GeneratorCommand;
use Illuminate\Support\Str;

class MakeRepositoryCommand extends GeneratorCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ant:gen:repository
    	{slug : The slug of the module}
    	{name : The name of the repository class}
    	{--not_resource : Generate a module resource repository class}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new module repository class';

    protected $container = ['name'=> ''];
    /**
     * String to store the command type.
     *
     * @var string
     */
    protected $type = 'Module repository';

    /**
     * Execute the console command.
     *
     * @return bool|null
     */
    public function fire()
    {
        $name = $this->parseName($this->getNameInput());
        if ($this->alreadyExists($this->getNameInput())) {
            $this->error($this->type.' already exists!');
            return false;
        }
        $files = $this->getStubs();
        foreach ($files as  $file => $path){
            $this->makeDirectory($path);
            $this->files->put($path, $this->build($name , $file));
        }
        $this->info($this->type.' created successfully.');
        $this->addMappingAndUsing($name);
    }

    /**
     * @param $name
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    protected function addMappingAndUsing($name){

        $module_provider_path = $this->getModuleProviderPath();
        $provider_content = $this->files->get($module_provider_path);
        $provider_content= $this->replaceMapping($provider_content, $name);
        $provider_content= $this->replaceUsing($provider_content, $name);
        $this->files->put($module_provider_path, $provider_content);
    }

    /**
     * @param $provider_content
     * @param $name
     * @return mixed
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    protected function replaceMapping($provider_content, $name){

        $mapping_source    =  __DIR__.'/stubs/repository/interfacemapping.stub';
        $mapping_replace_content = $this->files->get($mapping_source);
        $mapping_replace_content = $this->replaceClass($mapping_replace_content, $name);

        if(strpos($provider_content,$mapping_replace_content) === FALSE){
            $provider_content = str_replace('//DummyMapping', $mapping_replace_content.PHP_EOL."\t\t//DummyMapping", $provider_content);
        }
        
        return $provider_content;
    }

    /**
     * @param $provider_content
     * @param $name
     * @return mixed
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    protected function replaceUsing($provider_content, $name){
        $using_source    =  __DIR__.'/stubs/repository/using.stub';
        $using_replace_content = $this->files->get($using_source);
        $using_replace_content = $this->replaceNamespace($using_replace_content, $name)->replaceClass($using_replace_content, $name);

        if(strpos($provider_content,$using_replace_content) === FALSE){
            $provider_content = str_replace('//DummyUse', $using_replace_content.PHP_EOL.'//DummyUse', $provider_content);
        }

        return $provider_content;
    }

    /**
     * Get the destination Module Provider path.
     *
     * @return string
     */
    protected function getModuleProviderPath()
    {
        return module_path().'/'. Str::ucfirst($this->argument('slug')) . '/Providers/ModuleServiceProvider.php';
    }
    /**
     * Get the stub file for the generator.
     *
     * @return string
     */
    protected function getStubs()
    {
        $parse_name = $this->parseName(Str::ucfirst($this->getNameInput()));
        if ($this->option('not_resource')) {
            return [
                __DIR__.'/stubs/repository/repository.stub' => $this->getRepositoryPath($parse_name),
                __DIR__.'/stubs/repository/repository.interface.stub' => $this->getInterfacePath($parse_name),
            ];
        }
        return [
            __DIR__.'/stubs/repository/repository.resource.stub' => $this->getRepositoryPath($parse_name),
            __DIR__.'/stubs/repository/repository.interface.stub' => $this->getInterfacePath($parse_name),
        ];

    }
    /**
     * Get the stub file for the generator.
     *
     * @return string
     */
    protected function getStub()
    {
        if ($this->option('not_resource')) {
            return __DIR__.'/stubs/repository/repository.stub';
        }
        return __DIR__.'/stubs/repository/repository.resource.stub';

    }

    /**
     * @param $name
     * @param $file
     * @return string
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    protected function build($name, $file)
    {
        $stub = $this->files->get($file);

        $using_replace_content = $this->replaceNamespace($stub, $name)->replaceClass($stub, $name);
        return $this->replaceSlug($using_replace_content, $name);
    }

    /**
     * Get the default namespace for the class.
     *
     * @param  string  $rootNamespace
     * @return string
     */
    protected function getDefaultNamespace($rootNamespace)
    {
        return module_class(Str::slug($this->argument('slug')), 'Repositories\\'. Str::ucfirst(Str::camel($this->argument('name'))));
    }
    /**
     * Get the destination class path.
     *
     * @param  string  $name
     * @return string
     */
    protected function getRepositoryPath($name)
    {
        $rootNamespace = config('modules.namespace');
        $name = str_replace($rootNamespace, '', $name);
        return module_path().'/'.str_replace('\\', '/', Str::ucfirst(Str::camel($name))).'Repository.php';
    }
    /**
     * Get the destination class path.
     *
     * @param  string  $name
     * @return string
     */
    protected function getInterfacePath($name)
    {
        $rootNamespace = config('modules.namespace');
        logger('repositories.name:' . $name);
        $name = str_replace($rootNamespace, '', $name);
        logger('repositories:name' . Str::ucfirst(Str::camel($name)));
        return module_path().'/'.str_replace('\\', '/', Str::ucfirst(Str::camel($name))).'Interface.php';
    }

    /**
     * Replace the class name for the given stub.
     *
     * @param  string  $stub
     * @param  string  $name
     * @return string
     */
    protected function replaceClass($stub, $name)
    {
        $class = str_replace($this->getNamespace($name).'\\', '', $name);

        return str_replace('DummyClass', Str::ucfirst(Str::camel($class)), $stub);
    }

    /**
     * @param $stub
     * @param $name
     * @return mixed
     */
    protected function replaceSlug($stub, $name)
    {
        $class = str_replace($this->getNamespace($name).'\\', '', $name);
        return str_replace('DummySlug', Str::slug($class,'_'), $stub);
    }
    /**
     * Replace the namespace for the given stub.
     *
     * @param  string  $stub
     * @param  string  $name
     * @return $this
     */
    protected function replaceNamespace(&$stub, $name)
    {
        $stub = str_replace(
            'DummyNamespace', $this->getNamespace($name), $stub
        );

        $stub = str_replace(
            'DummyRootNamespace', $this->laravel->getNamespace(), $stub
        );

        $stub = str_replace(
            'DummyModuleNamespace', module_class(Str::slug($this->argument('slug')),''), $stub
        );

        return $this;
    }
}
