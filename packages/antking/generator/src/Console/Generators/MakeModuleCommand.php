<?php

namespace Antking\Generator\Console\Generators;

use Illuminate\Filesystem\Filesystem;
use Symfony\Component\Console\Helper\ProgressBar;

class MakeModuleCommand extends \Caffeinated\Modules\Console\Generators\MakeModuleCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ant:gen:module
        {slug : The slug of the module}
        {--Q|quick : Skip the make:module wizard and use default values}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new Caffeinated module and bootstrap it';

    /**
     * Array to store the configuration details.
     *
     * @var array
     */
    protected $container;

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $this->container['slug']        = str_slug($this->argument('slug'));
        $this->container['name']        = studly_case($this->container['slug']);
        $this->container['version']     = '1.0';
        $this->container['description'] = 'This is the description for the '.$this->container['name'].' module.';

        if ($this->option('quick')) {
            $this->container['basename']    = studly_case($this->container['slug']);
            $this->container['namespace']   = config('modules.namespace').$this->container['basename'];
            return $this->generate();
        }

        $this->displayHeader('make_module_introduction');

        $this->stepOne();
    }

    /**
     * Step 1: Configure module manifest.
     *
     * @return mixed
     */
    private function stepOne()
    {
        $this->displayHeader('make_module_step_1');

        $this->container['name']        = $this->ask('Please enter the name of the module:', $this->container['name']);
        $this->container['slug']        = $this->ask('Please enter the slug for the module:', $this->container['slug']);
        $this->container['version']     = $this->ask('Please enter the module version:', $this->container['version']);
        $this->container['description'] = $this->ask('Please enter the description of the module:', $this->container['description']);
        $this->container['basename']    = studly_case($this->container['slug']);
        $this->container['namespace']   = config('modules.namespace').$this->container['basename'];

        $this->comment('You have provided the following manifest information:');
        $this->comment('Name:                       '.$this->container['name']);
        $this->comment('Slug:                       '.$this->container['slug']);
        $this->comment('Version:                    '.$this->container['version']);
        $this->comment('Description:                '.$this->container['description']);
        $this->comment('Basename (auto-generated):  '.$this->container['basename']);
        $this->comment('Namespace (auto-generated): '.$this->container['namespace']);

        if ($this->confirm('Do you wish to continue?')) {
            $this->comment('Thanks! That\'s all we need.');
            $this->comment('Now relax while your module is generated.');

            $this->generate();
        } else {
            return $this->stepOne();
        }

        return true;
    }


    /**
     * Generate defined module folders.
     */
    protected function generateModule()
    {
        if (!$this->files->isDirectory(module_path())) {
            $this->files->makeDirectory(module_path());
        }

        $directory = module_path(null, $this->container['basename']);
        $source    = __DIR__.'/../../../resources/stubs/module';

        $this->files->makeDirectory($directory);
        $this->files->copyDirectory($source, $directory, null);

        $files = $this->files->allFiles($directory);

        foreach ($files as $file) {
            $contents = $this->replacePlaceholders($file->getContents());
            $filePath = module_path(null, $this->container['basename'].'/'.$file->getRelativePathname());

            $this->files->put($filePath, $contents);
        }
    }
}
