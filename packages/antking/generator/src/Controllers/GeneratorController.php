<?php

/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 10/26/2016
 * Time: 5:24 PM
 */
namespace Antking\Generator\Controllers;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
class GeneratorController extends Controller
{
    public function index()
    {
        event('blog.module.made');
        return view('views::generator.index',
            [
                'f1Name' => 'Blog',
                'f1Slug' => 'blog',
                'f1Version' => '1.0.0',
                'f1Description' => 'Blog',
            ]);
    }
}