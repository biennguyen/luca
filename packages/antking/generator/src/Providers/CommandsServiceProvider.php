<?php

namespace Antking\Generator\Providers;

use Illuminate\Support\ServiceProvider;

class CommandsServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     */
    public function register()
    {
        $generators = [
            'ant.gen'            => \Antking\Generator\Console\Generators\MakeCommand::class,
            'ant.gen.model'            => \Antking\Generator\Console\Generators\MakeModelCommand::class,
            'ant.gen.controller'            => \Antking\Generator\Console\Generators\MakeControllerCommand::class,
            'ant.gen.module'            => \Antking\Generator\Console\Generators\MakeModuleCommand::class,
            'ant.gen.repository'            => \Antking\Generator\Console\Generators\MakeRepositoryCommand::class,
        ];

        foreach ($generators as $slug => $class) {
            $this->app->singleton($slug, function ($app) use ($slug, $class) {
                return $app[$class];
            });

            $this->commands($slug);
        }
    }
}
