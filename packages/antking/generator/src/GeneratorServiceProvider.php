<?php

namespace Antking\Generator;

use Illuminate\Support\ServiceProvider;
use Antking\Generator\Providers\CommandsServiceProvider;
use Antking\Generator\Providers\LibrariesServiceProvider;

class GeneratorServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        $this->loadTranslationsFrom(__DIR__ . '/../translations', 'generator');
        if (! $this->app->routesAreCached()) {
            require __DIR__ . '/../routes/routes.php';
        }
        $this->loadViewsFrom(__DIR__ . '/../resources/views', 'views');
        $this->publishes([
            __DIR__ . '/../config/generator.php' => config_path('generator.php'),
            __DIR__ . '/../translations' => resource_path('lang/vendor/generator'),
            __DIR__.'/../resources/views' => base_path('resources/views/antking/generator'),
            __DIR__.'/../public' => base_path('public/assets'),
        ]);
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
        $this->mergeConfigFrom(
            __DIR__ . '/../config/generator.php', 'generator'
        );
        $this->app->make('Antking\Generator\Controllers\GeneratorController');
        $this->app->register(CommandsServiceProvider::class);
    }
}
