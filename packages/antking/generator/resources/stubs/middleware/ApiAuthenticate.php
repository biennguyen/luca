<?php

namespace App\Http\Middleware;

use App\Model\UserToken;
use App\Model\Common\HttpCode;
use App\Model\Common\Result;
use Cartalyst\Sentinel\Sentinel;
use Closure;
use Illuminate\Http\JsonResponse;

class ApiAuthenticate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->header('token') !== null){
            $tokenString = $request->header('token');
            $token = UserToken::where('code', '=', $tokenString)->with('user')->first();
            if ($token !== null) {
                $sentinel = \App::make(Sentinel::class);

                $user = $token->user()->first();
                $sentinel->login($user);
                return $next($request);
            }
        }

        return Result::direct(false, 'Unauthorized.', HttpCode::Unauthorized);
    }
}
