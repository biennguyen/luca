@extends('views::layouts.layout')
@section('content')
    <div class="row">
        <div class="col-sm-8 col-sm-offset-2 text">
            <h1>Antking <strong>Generating</strong> Wizard</h1>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3 form-box">
            <form role="form" action="" method="post" class="f1">
                <h3>Register New Module</h3>
                <div class="f1-steps">
                    <div class="f1-progress">
                        <div class="f1-progress-line" data-now-value="16.66" data-number-of-steps="3" style="width: 16.66%;"></div>
                    </div>
                    <div class="f1-step active">
                        <div class="f1-step-icon"><i class="fa fa-user"></i></div>
                        <p>Module</p>
                    </div>
                    <div class="f1-step">
                        <div class="f1-step-icon"><i class="fa fa-key"></i></div>
                        <p>Model</p>
                    </div>
                    <div class="f1-step">
                        <div class="f1-step-icon"><i class="fa fa-twitter"></i></div>
                        <p>social</p>
                    </div>
                </div>
                <fieldset>
                    <div class="form-group" >
                        <label class="sr-only" for="f1-name">Name</label>
                        <input type="text" readonly="readonly" value="{{ $f1Name }}" name="f1-name" placeholder="Module name..." class="f1-first-name form-control read-only" id="f1-name">
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="f1-slug">slug</label>
                        <input type="text" readonly="readonly" value="{{$f1Slug}}" name="f1-slug" placeholder="Slug..." class="f1-last-name form-control" id="f1-slug">
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="f1-version">version</label>
                        <input type="text" readonly="readonly"  value="{{$f1Version}}" name="f1-version" placeholder="Version..." class="f1-last-name form-control" id="f1-version">
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="f1-description">Description</label>
                        <textarea name="f1-description" readonly="readonly" placeholder="About module..."
                                  class="f1-about-yourself form-control" id="f1-description">{{$f1Description}}</textarea>
                    </div>
                    <div class="f1-buttons">
                        <button type="button" class="btn btn-next">Next</button>
                    </div>
                </fieldset>

                <fieldset>
                    <h4>Set up your MODEL:</h4>
                    <div class="form-group">
                        <label class="sr-only" for="f2Name">Name</label>
                        <input type="text" name="f2Name" placeholder="Name..." class="f1-email form-control" id="f2Name">
                    </div>
                    <div  class="form-group" data-toggle="buttons">
                        <label class="btn btn-info active time_stamp">
                            <input class="form-control" type="checkbox" checked="checked" id="f2TimeStamp" name="f2TimeStamp" />
                        </label>
                        <label class="btn btn-success active pull-right soft_delete">
                            <input  type="checkbox" id="f2SoftDelete" checked="checked" name="f2SoftDelete" />
                        </label>
                    </div>

                    <div class="form-group">
                        <div class="crud-table panel panel-default">
                            <!-- Default panel contents -->
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col col-xs-6">
                                        <h3 class="panel-title">Fields</h3>
                                    </div>
                                    <div class="col col-xs-6 text-right">
                                        <button type="button" class="btn btn-sm btn-primary btn-create">Create New</button>
                                    </div>
                                </div>
                            </div>

                            <!-- Table -->
                            <div class="panel-body">
                                <table class="table table-striped table-bordered table-sortable table-list">
                                    <thead>
                                    <tr>
                                        <th class="hidden-xs">ID</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th><em class="fa fa-cog"></em></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td class="hidden-xs">1</td>
                                        <td><input type="text" value="name 2" class="form-control"></td>
                                        <td><input type="text" value="email 2" class="form-control"></td>
                                        <td>
                                            <a class="btn btn-danger"><em class="fa fa-trash"></em></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="hidden-xs">2</td>
                                        <td><input type="text" value="name 2" class="form-control"></td>
                                        <td><input type="text" value="email 2" class="form-control"></td>
                                        <td>
                                            <a class="btn btn-danger"><em class="fa fa-trash"></em></a>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                    <div class="form-group pull-right">
                        <button type="button" class="btn btn-previous">Previous</button>
                        <button type="button" class="btn btn-next">Next</button>
                    </div>
                </fieldset>

                <fieldset>
                    <h4>Social media profiles:</h4>
                    <div class="form-group">
                        <label class="sr-only" for="f1-facebook">Facebook</label>
                        <input type="text" name="f1-facebook" placeholder="Facebook..." class="f1-facebook form-control" id="f1-facebook">
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="f1-twitter">Twitter</label>
                        <input type="text" name="f1-twitter" placeholder="Twitter..." class="f1-twitter form-control" id="f1-twitter">
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="f1-google-plus">Google plus</label>
                        <input type="text" name="f1-google-plus" placeholder="Google plus..." class="f1-google-plus form-control" id="f1-google-plus">
                    </div>
                    <div class="f1-buttons">
                        <button type="button" class="btn btn-previous">Previous</button>
                    </div>
                </fieldset>
                <button type="submit" class="btn btn-submit">Submit</button>
            </form>
        </div>
    </div>
@stop